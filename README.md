# A quest for Knowledge #
In my spare time I developed a pretty simple incremental game called *A quest for knowledge*, where you impersonate a man on his quest to accumulate data from every corner of the universe. It has a sci-fi taste, with several jokes and easter eggs scattered around (if you can find them :p).

# Description #

The game features one primary resource, **bytes**, which is your main currency and which guides the incremental aspect of the game; and one secondary resource, flops, earned mostly through achievements. Anyway, the code structure supports any kind of additional currencies that may be added in the future. Apart from clicking, which is a minor part of the game, you can build **devices** that collect information in your stead. These can be **upgraded** by spending resources. Unlike most incremental games, upgrades feature many different mechanics other than simply multiplying your earnings. Additionally, you will find **items** that manipulate the status of the game, transforming devices into other more interesting things, sweeping them away or applying bonuses/maluses that decay over time. The ultimate goal of the game is to reach the final prestige league, which is not a very simple task!
This is a beta version of the game, but everything that I exposed before has been implemented. At the moment there are only a handful of items to use, a total of 20 devices (with related upgrades), and something around 100 achievements to earn.

## Followup ##
If you are interested in adding new features to the game, please comment on its related forum. If you find a bug, post it on the forum as well. If you feel like contribuiting in any way, do the same! Main discussion is on [reddit](https://www.reddit.com/r/incremental_games/comments/3nzrrx/a_quest_for_knowledge/).

# Changelog #
* Implemented grid-layout graphics and tuned some upgrades and items.
* Reworked Power Core, Scolding, Arcaic Reading, Universal Catalyst and Hard Times. Increased the cost of Cogversion. Also I found a new free host for the game.