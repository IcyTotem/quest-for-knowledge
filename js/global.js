﻿function isFunction(obj) {
    return (typeof obj) == "function";
}

function isString(obj) {
    return (typeof obj) == "string";
}

function isArray(obj) {
    return obj instanceof Array;
}

function isUndefined(obj) {
    return (typeof obj) == "undefined";
}

function isObject(obj) {
    return (typeof obj) == "object";
}


function now() {
    return (new Date()).getTime() / 1000;
}

function interval(seconds, f) {
    return setInterval(f, seconds * 1000);
}

function timeout(seconds, f) {
    return setTimeout(f, seconds * 1000);
}


var ASCENDING = true;
var DESCENDING = false;
var UNSORTED = null;

var ln10 = Math.log(10);

Math.log10 = function (x) {
    return Math.log(x) / ln10;
};

Math.geometricSum = function (r, n) {
    if (r == 1)
        return n;
    else
        return (1 - Math.pow(r, n)) / (1 - r);
};

// [min, max)
Math.randomInt = function (min, max) {
    var delta = Math.floor(Math.random() * (max - min));

    if (delta == (max - min))
        delta = 0;

    return min + delta;
};


Date.prototype.isToday = function () {
    var now = new Date();
    return (this.getDate() == now.getDate()) && (this.getMonth() == now.getMonth()) && (this.getYear() == now.getYear());
};

Date.prototype.isYesterday = function () {
    var now = new Date();
    var yeserday = new Date();
    yeserday.setDate(now.getDate() - 1);
    return (this.getDate() == yeserday.getDate()) && (this.getMonth() == yeserday.getMonth()) && (this.getYear() == yeserday.getYear());
};