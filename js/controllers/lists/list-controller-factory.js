﻿define([], function () {
    var factory = {
        itemClass: ".item",

        create: function ($container, collection) {
            var $parent = $container.parent();
            var $emptyPlaceholder = $parent.find(".empty");

            var $original = $container.find(this.itemClass);
            var $template = $original.clone();
            var itemClass = this.itemClass;
            var bindings = [];

            collection.forEach(function (object) {
                var $item = $template.clone();
                $container.append($item);
                bindings.push({ $item: $item, object: object });
            });

            $original.hide();
            $container.hide();
            $emptyPlaceholder.show();

            var result = {
                bindinds: bindings,
                sorting: [],

                populate: function ($item, object) {
                    // To be implemented by clients
                },

                isVisible: function (object) {
                    // To be implemented by clients
                    return true;
                },

                isEnabled: function (object) {
                    // To be implemented by clients
                    return true;
                },

                onDiscover: function (object) {
                    // To be implemented by clients
                },

                hideAll: function () {
                    bindings.forEach(function (binding) { binding.$item.hide(); });
                },

                update: function () {
                    if (!$container.is(":visible"))
                        return;

                    var that = this;

                    bindings.forEach(function (binding) {
                        that.populate(binding.$item, binding.object);
                    });
                },

                refresh: function () {
                    var that = this;

                    bindings.forEach(function (binding) {
                        var objectVisible = that.isVisible(binding.object);
                        var objectEnabled = that.isEnabled(binding.object);
                        var $itemVisible = (binding.$item.css("display") != "none");

                        if (objectEnabled)
                            binding.$item.removeClass("disabled");
                        else
                            binding.$item.addClass("disabled");

                        if (!$itemVisible && objectVisible) {
                            if (!binding.object.discovered) that.onDiscover(binding.object);
                            binding.object.discovered = true;
                            binding.$item.fadeIn();
                        }

                        var $containerVisible = ($container.css("display") != "none");

                        if (!$containerVisible && objectVisible) {
                            $emptyPlaceholder.hide();
                            $container.show();
                        }
                    });
                },

                invalidate: function () {
                    this.update();
                    this.refresh();
                },

                sort: function (attrName) {
                    var prevSorting = this.sorting[attrName];

                    for (var key in this.sorting)
                        this.sorting[key] = UNSORTED;

                    if (isUndefined(prevSorting) || (prevSorting == UNSORTED))
                        this.sorting[attrName] = DESCENDING;
                    else
                        this.sorting[attrName] = !prevSorting;

                    var before = (this.sorting[attrName] == ASCENDING ? -1 : 1);
                    var after = -before;

                    $container.find(itemClass).sort(function (e0, e1) {
                        var rank0 = Number($(e0).attr(attrName));
                        var rank1 = Number($(e1).attr(attrName));

                        if (rank0 < rank1)
                            return before;
                        else
                            return after;
                    }).detach().appendTo($container);
                }
            };

            return result;
        }
    };

    return factory;
});