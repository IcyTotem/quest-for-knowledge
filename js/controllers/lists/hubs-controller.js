﻿define([
    "controllers/lists/list-controller-factory",
    "controllers/widgets/score-controller",
    "models/instances/hubs",
    "core/format",
    "models/instances/resources",
    "models/containers/notifications"
],
function (factory, scoreController, hubs, format, resources, notifications) {
    var $container = $("#devices .devices .list");
    var $miniMenu = $("#devices .devices .mini-menu");
    var hubsController = factory.create($container, hubs.all);

    var stackSizes = [1, 10, 100];
    var stackIndex = 0;
    var stack = 1;

    hubsController.isVisible = function (hub) {
        return (hub.count > 0) || (hub.device.baseCost <= hub.device.currency.count * hub.device.currency.discoveryRange);
    };

    hubsController.isEnabled = function (hub) {
        return (hub.stackCost() <= hub.device.currency.count);
    };

    hubsController.onDiscover = function (hub) {
        notifications.push({
            image: "images/devices/" + hub.device.rank + ".png",
            message: "You discovered a new <em>device</em>: the <strong>" + hub.device.name + "</strong>!"
        });
    };

    hubsController.populate = function ($item, hub) {
        $item.find(".icon").attr("src", "images/devices/" + hub.device.rank + ".png");
        $item.find(".title").text(hub.device.name);
        $item.find(".bps").text(format.number(hub.device.bps(), "B/s"));
        $item.find(".cost").text(format.number(hub.stackCost(), hub.device.currency.unit));
        $item.find(".count").text(hub.count);
        $item.find(".tooltip").html(
            hub.device.description + "<br/>" +
            "Your " + hub.device.name + "s transfer <strong>" + format.number(hub.bps(), "B/s") + "</strong> in total.<br/>" +
            "<em>(" + hub.device.type + ")</em>"
        );

        $item.attr("data-rank", hub.device.rank);
        $item.attr("data-cost", hub.cost(1));
        $item.attr("data-count", hub.count);
    };

    hubsController.bindinds.forEach(function (binding) {
        var $item = binding.$item;
        var hub = binding.object;

        $item.find(".buy").click(function () {
            if (!hub.buy(stack))
                return;

            scoreController.update();
            hubsController.populate($item, hub);
            hubsController.invalidate();
        });
    });


    $miniMenu.find(".sort-by-rank").click(function () {
        hubsController.sort("data-rank");
    });
    $miniMenu.find(".sort-by-cost").click(function () {
        hubsController.sort("data-cost");
    });
    $miniMenu.find(".sort-by-count").click(function () {
        hubsController.sort("data-count");
    });
    $miniMenu.find(".increase-stack").click(function () {
        stackIndex = (stackIndex + 1) % stackSizes.length;
        stack = stackSizes[stackIndex];

        hubs.all.forEach(function (hub) { hub.stack = stack; });
        $(this).find(".stack").text(stack);

        hubsController.invalidate();
    });


    hubsController.hideAll();
    hubsController.invalidate();

    return hubsController;
});