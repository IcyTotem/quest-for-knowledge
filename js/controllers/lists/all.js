﻿define([
    "controllers/lists/hubs-controller",
    "controllers/lists/inventory-controller",
    "controllers/lists/resources-controller",
    "controllers/lists/shop-controller",
    "controllers/lists/upgrades-controller"
], function (hubs, inventory, resources, shop, upgrades) {
    return {
        hubs: hubs,
        inventory: inventory,
        resources: resources,
        shop: shop,
        upgrades: upgrades
    };
});