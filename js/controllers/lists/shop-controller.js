﻿define([
    "controllers/lists/list-controller-factory",
    "models/instances/items",
    "models/instances/resources",
    "models/containers/notifications",
    "controllers/widgets/score-controller",
    "core/format"
],
function (factory, items, resources, notifications, scoreController, format) {
    var $container = $("#shop .shop .list");
    var shopController = factory.create($container, items.all.filter(function (item) { return item.purchasable; }));

    shopController.isVisible = function (item) {
        return (item.baseCost <= item.currency.count * item.currency.discoveryRange);
    };

    shopController.isEnabled = function (item) {
        return (item.stackCost() <= item.currency.count);
    };

    shopController.onDiscover = function (item) {
        notifications.push({
            image: "images/items/" + item.id + ".png",
            message: "You discovered new items: <strong>" + item.name + "</strong>!"
        });
    };

    shopController.populate = function ($item, item) {
        $item.find(".icon").attr("src", "images/items/" + item.id + ".png");
        $item.find(".title").text(item.name);
        $item.find(".effect").html(item.description);
        $item.find(".tooltip").html(item.details());
        $item.find(".cost").text(format.number(item.cost(1), item.currency.unit));
        $item.find(".count").text(item.count);
    };

    shopController.bindinds.forEach(function (binding) {
        var $item = binding.$item;
        var item = binding.object;

        $item.find(".buy").click(function () {
            if (!item.buy(1))
                return;

            scoreController.update();
            shopController.populate($item, item);
            shopController.refresh();
        });
    });

    shopController.hideAll();
    shopController.invalidate();

    return shopController;
});