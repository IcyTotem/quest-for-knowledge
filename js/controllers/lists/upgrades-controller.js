﻿define([
    "controllers/lists/list-controller-factory",
    "controllers/widgets/score-controller",
    "models/instances/upgrades",
    "core/format",
    "models/instances/resources",
    "models/containers/notifications"
],
function (factory, scoreController, upgrades, format, resources, notifications) {
    var $container = $("#upgrades .upgrades .list");
    var $miniMenu = $("#upgrades .upgrades .mini-menu");
    var upgradesController = factory.create($container, upgrades.all);

    var levelDeltas = [1, 5, 10];
    var deltaIndex = 0;
    var delta = 1;

    upgradesController.isVisible = function (upgrade) {
        return (upgrade.level > 0) || (upgrade.baseCost <= upgrade.target.device.currency.count * upgrade.target.device.currency.discoveryRange);
    };

    upgradesController.isEnabled = function (upgrade) {
        return (upgrade.deltaCost() <= upgrade.target.device.currency.count);
    };

    upgradesController.onDiscover = function (upgrade) {
        notifications.push({
            image: "images/devices/" + upgrade.target.device.rank + ".png",
            message: "You discovered a new <em>upgrade</em>: <strong>" + upgrade.name + "</strong>!"
        });
    };

    upgradesController.populate = function ($item, upgrade) {
        $item.find(".icon").attr("src", "images/devices/" + upgrade.target.device.rank + ".png");
        $item.find(".title").text(upgrade.name);
        $item.find(".effect").html(upgrade.description(upgrade.level));
        $item.find(".cost").text(format.number(upgrade.deltaCost(), upgrade.target.device.currency.unit));
        $item.find(".level").text(upgrade.level);
        $item.find(".tooltip").html(
            "<em>(Current level)</em> " + upgrade.description(upgrade.level) +
            "<br/>" +
            "<em>(Next level)</em> " + upgrade.description(upgrade.level + 1)
        );

        $item.attr("data-rank", upgrade.target.device.rank);
        $item.attr("data-cost", upgrade.cost());
        $item.attr("data-level", upgrade.level);
    };

    upgradesController.bindinds.forEach(function (binding) {
        var $item = binding.$item;
        var upgrade = binding.object;

        $item.find(".buy").click(function () {
            if (!upgrade.buy(delta))
                return;

            scoreController.update();
            upgradesController.populate($item, upgrade);
            upgradesController.refresh();
        });
    });


    $miniMenu.find(".sort-by-rank").click(function () {
        upgradesController.sort("data-rank");
    });
    $miniMenu.find(".sort-by-cost").click(function () {
        upgradesController.sort("data-cost");
    });
    $miniMenu.find(".sort-by-level").click(function () {
        upgradesController.sort("data-level");
    });
    $miniMenu.find(".increase-delta").click(function () {
        deltaIndex = (deltaIndex + 1) % levelDeltas.length;
        delta = levelDeltas[deltaIndex];

        upgrades.all.forEach(function (upgrade) { upgrade.delta = delta; });
        $(this).find(".delta").text(delta);

        upgradesController.invalidate();
    });


    upgradesController.hideAll();
    upgradesController.invalidate();

    return upgradesController;
});