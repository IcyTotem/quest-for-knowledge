﻿define([
    "controllers/lists/list-controller-factory",
    "models/instances/resources",
    "models/containers/notifications",
    "core/format"
],
function (factory, resources, notifications, format) {
    var $container = $("#resources .list");
    var resourcesController = factory.create($container, resources.all);

    resourcesController.isVisible = function (resource) {
        return (resource.count > 0);
    };

    resourcesController.isEnabled = function (resource) {
        return (resource.count != 0);
    };

    resourcesController.onDiscover = function (resource) {
        notifications.push({
            image: "images/resources/" + resource.id + ".png",
            message: "You gained some <strong>" + resource.name + "</strong>!"
        });
    };

    resourcesController.populate = function ($item, resource) {
        $item.find(".icon").attr("src", "images/resources/" + resource.id + ".png");
        $item.find(".title").text(resource.name);
        $item.find(".effect").html(resource.description);
        $item.find(".tooltip").html(resource.details());
        $item.find(".res-count").text(resource.countAsString());
    };

    resourcesController.hideAll();
    resourcesController.invalidate();

    return resourcesController;
});