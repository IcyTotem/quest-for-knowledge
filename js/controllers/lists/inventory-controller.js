define([
    "controllers/lists/list-controller-factory",
    "models/instances/items",
    "models/instances/resources",
    "models/containers/notifications"
],
function (factory, items, resources, notifications) {
    var $container = $("#inventory .inventory .list");
    var $miniMenu = $("#inventory .mini-menu");
    var inventoryController = factory.create($container, items.all);

    var stackSizes = [1, 10, 100];
    var stackIndex = 0;
    var stack = 1;

    inventoryController.isVisible = function (item) {
        return (item.count > 0) || (item.effect && item.effect.enabled);
    };

    inventoryController.isEnabled = function (item) {
        return ((item.count != 0) && (item.stack <= item.count)) || (item.effect && item.effect.enabled);
    };

    inventoryController.onDiscover = function (item) {
        notifications.push({
            image: "images/items/" + item.id + ".png",
            message: "You " + ((item.purchasable && (item.count == 0)) ? "discovered" : "acquired") + " new items: <strong>" + item.name + "</strong>!"
        });
    };

    inventoryController.populate = function ($item, item) {
        $item.find(".icon").attr("src", "images/items/" + item.id + ".png");
        $item.find(".title").text(item.name);
        $item.find(".effect").html(item.description);
        $item.find(".tooltip").html(item.details());
        $item.find(".count").text(item.count);

        if (!item.effect)
            return;

        var $progress = $item.find(".progress");
        
        if (!item.effect.enabled) {
            if ($progress.is(":visible"))
                $progress.fadeOut();
        } else {
            if (!$progress.is(":visible"))
                $progress.fadeIn();

            $progress.find(".progress-label").text(item.effect.label());
            $progress.find(".progress-bar").css({ width: (item.effect.progress() * 100).toFixed(0) + "%" });
        }
    };

    inventoryController.bindinds.forEach(function (binding) {
        var $item = binding.$item;
        var item = binding.object;

        $item.find(".use").click(function () {
            item.use(stack);
            inventoryController.populate($item, item);
            inventoryController.refresh();
        });
    });


    $miniMenu.find(".increase-stack").click(function () {
        stackIndex = (stackIndex + 1) % stackSizes.length;
        stack = stackSizes[stackIndex];

        items.all.forEach(function (item) { item.stack = stack; });
        $(this).find(".stack").text(stack);

        inventoryController.invalidate();
    });


    inventoryController.hideAll();
    inventoryController.invalidate();

    return inventoryController;
});