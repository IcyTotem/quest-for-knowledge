﻿define(["controllers/lists/all"], function (lists) {
    var tabController = {
        attach: function () {
            var $container = $("#tab-headers");
            var $tabHeaders = $(".tab-header");
            var $tabContents = $(".tab-content");
            var that = this;

            $tabHeaders.click(function () {
                var targetId = $(this).attr("data-id");

                $tabHeaders.removeClass("selected unselected").addClass("unselected");
                $(this).removeClass("unselected").addClass("selected");

                $tabContents.hide();
                $(targetId).show();

                lists.hubs.invalidate();
                lists.upgrades.invalidate();
                lists.resources.invalidate();
                lists.inventory.invalidate();
                lists.shop.invalidate();
            });
        }
    };

    return tabController;
});