﻿define([
    "core/session",
    "models/instances/all",
    "models/containers/all",
    "models/singleton/all"
], function (session, instances, containers, singleton) {
    var optionsController = { };
    var $body = $("body");

    $("#toggle-moving-background").click(function () {
        if ($body.hasClass("fixed-background")) {
            $body.removeClass("fixed-background");
            $body.addClass("moving-background");
        } else {
            $body.removeClass("moving-background");
            $body.addClass("fixed-background");
        }
    });

    var $saveOverlay = $("#save-game-overlay");
    var $loadOverlay = $("#load-game-overlay");
    var $newOverlay = $("#new-game-overlay");

    $("#save-game").click(function () {
        var data =
            $.base64.encode(JSON.stringify(instances.devices)) + "|" +
            $.base64.encode(JSON.stringify(instances.hubs)) + "|" +
            $.base64.encode(JSON.stringify(instances.items)) + "|" +
            $.base64.encode(JSON.stringify(instances.resources)) + "|" +
            $.base64.encode(JSON.stringify(instances.upgrades)) + "|" +
            $.base64.encode(JSON.stringify(containers.achievements)) + "|" +
            $.base64.encode(JSON.stringify(containers.notifications)) + "|" +
            $.base64.encode(JSON.stringify(singleton.clicker)) + "|" +
            $.base64.encode(JSON.stringify(singleton.player));

        $saveOverlay.find("textarea").val(data);
        $saveOverlay.fadeIn();
    });
    $saveOverlay.find(".ok").click(function () {
        $saveOverlay.fadeOut();
    });

    $("#load-game").click(function () {
        $loadOverlay.fadeIn();
    });
    $loadOverlay.find(".cancel").click(function () {
        $loadOverlay.fadeOut();
    });
    $loadOverlay.find(".ok").click(function () {
        var data = $loadOverlay.find("textarea").val();
        var parts = data.split("|");

        if (parts.length != 9)
            return alert("The data you provided is either corrupt, incomplete or comes from a different game version!");

        try {
            var parsedObjects = [];

            for (var i = 0; i < parts.length; ++i)
                parsedObjects[i] = JSON.parse($.base64.decode(parts[i]));
            
            instances.devices.load(parsedObjects[0]);
            instances.hubs.load(parsedObjects[1]);
            instances.items.load(parsedObjects[2]);
            instances.resources.load(parsedObjects[3]);
            instances.upgrades.load(parsedObjects[4]);
            containers.achievements.load(parsedObjects[5]);
            containers.notifications.load(parsedObjects[6]);
            singleton.clicker.load(parsedObjects[7]);
            singleton.player.load(parsedObjects[8]);
        } catch (e) {
            console.log(e);
            return alert("The data you provided is either corrupt, incomplete or comes from a different game version!");
        }

        $loadOverlay.fadeOut();
    });

    $("#new-game").click(function () {
        $newOverlay.fadeIn();
    });
    $newOverlay.find(".yes").click(function () {
        session.clear();
        singleton.player.reset = true;
        location.reload();
    });
    $newOverlay.find(".no").click(function () {
        $newOverlay.fadeOut();
    });

    return optionsController;
});