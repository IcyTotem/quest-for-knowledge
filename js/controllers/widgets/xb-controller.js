﻿define([], function () {
    // Script execution enabled
    $("#script-warning").hide();

    var $lsWarning = $("#local-storage-warning");
    var $tabArea = $("#tab-area");

    // Local storage support
    if (!localStorage)
        $lsWarning.show();
    else 
        $tabArea.show();

    // Manual saves
    $("#continue-save-manually").click(function () {
        $lsWarning.hide();
        $tabArea.show();
    });

    // Pushable dynamic behaviour
    $(".pushable").click(function () {
        var $element = $(this);

        $element.parent().find(".pushed").removeClass("pushed");
        $element.addClass("pushed");
    });
});