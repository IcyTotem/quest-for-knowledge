﻿define([
    "controllers/widgets/clicker-controller",
    "controllers/widgets/notifications-controller",
    "controllers/widgets/options-controller",
    "controllers/widgets/score-controller",
    "controllers/widgets/tab-controller",
    "controllers/widgets/tooltip-controller",
    "controllers/widgets/stats-controller",
    "controllers/widgets/xb-controller"
], function (clicker, notifications, options, score, tab, tooltip, stats, xb) {
    return {
        clicker: clicker,
        notifications: notifications,
        options: options,
        score: score,
        tab: tab,
        tooltip: tooltip,
        stats: stats,
        xb: xb
    };
});