﻿define([], function () {
    var tooltipController = {
        enabled: true,
        displacement: { x: 0, y: 15 },

        attach: function () {
            var $mainTooltip = $("#main-tooltip");
            var $targetElements = $("*[tooltip-enabled='true']");

            $targetElements.mouseenter(function () {
                var $target = $(this);
                var content = $target.parent().find(".tooltip").html();
                var centered = $target.attr("tooltip-centered");
                var targetOffset = $target.offset();

                $mainTooltip.html(content);

                if (centered) {
                    $mainTooltip.css({
                        top: targetOffset.top - $(window).scrollTop()  + $target.height() + tooltipController.displacement.y,
                        left: targetOffset.left + ($target.width() - $mainTooltip.width()) / 2,
                        "text-align": "center"
                    });
                } else {
                    $mainTooltip.css({
                        top: targetOffset.top - $(window).scrollTop() + $target.height() + tooltipController.displacement.y,
                        left: targetOffset.left + tooltipController.displacement.x,
                        "text-align": "left"
                    });
                }

                if (tooltipController.enabled)
                    $mainTooltip.show();
            }).mouseleave(function () {
                $mainTooltip.hide();
            });
        }
    };

    return tooltipController;
});