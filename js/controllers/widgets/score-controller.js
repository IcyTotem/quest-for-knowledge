﻿define([
    "models/instances/resources",
    "models/instances/hubs",
    "models/singleton/player",
    "core/format",
    "core/large-numbers"
],
function (resources, hubs, player, format, largeNumbers) {
    var $counter = $("#counter");
    var $counterWord = $counter.parent().find(".tooltip .word");

    var $bps = $("#bps");
    var $bpsWord = $bps.parent().find(".tooltip .word");

    var $endOverlay = $("#end-game-overlay");
    var endOverlayShowed = false;

    var scoreController = {
        update: function () {
            var winByCoper = (player.league.order >= largeNumbers.prefixes.coper.order);
            var winByNan = (resources.bytes.count == Infinity) || (resources.bytes.count != resources.bytes.count);
            var win = winByCoper || winByNan;

            if (!win || endOverlayShowed) {
                $counter.text(resources.bytes.countAsString(4));
                $bps.text(format.number(hubs.all.bps(), "B/s", 4));
            } else {
                if (winByCoper) {
                    $endOverlay.find(".coper").show();
                    $endOverlay.find(".nan").hide();
                } else {
                    $endOverlay.find(".coper").hide();
                    $endOverlay.find(".nan").show();
                }

                if (!endOverlayShowed) {
                    endOverlayShowed = true;
                    $endOverlay.find(".playtime").text(format.time(player.playtime()));
                    $endOverlay.fadeIn();
                }
            } 
        },

        updateTooltips: function () {
            $counterWord.text(format.word(resources.bytes.count, "bytes", 4));
            $bpsWord.text(format.word(hubs.all.bps(), "bytes per second", 4));
        }
    };

    $counter.mouseenter(scoreController.updateTooltips);
    $bps.mouseenter(scoreController.updateTooltips);

    $endOverlay.find(".ok").click(function () {
        $endOverlay.fadeOut();
    });

    return scoreController;
});