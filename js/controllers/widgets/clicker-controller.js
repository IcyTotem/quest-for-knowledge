﻿define([
    "core/format",
    "models/singleton/clicker",
    "models/singleton/player",
    "controllers/widgets/score-controller"
],
function (format, clicker, player, scoreController) {
    var clickerController = {
        enabled: true
    };

    var $clicker = $("#clicker");
    var $clickAmount = $("#click-amount");
    var $both = $("#clicker, #click-amount");

    var faded = false;
    var waitHandle;

    // Hack to center the span on its parent div
    $clickAmount.css({ "line-height": $clickAmount.parent().height() + "px" });

    // Both targets' events are handled since there are click-through problems 
    // with the span z-index, which must be greater than 0 to overlay on the image.
    // Anyway, modern browsers ignore the click events on #click-amount because of 
    // pointer-events: none in its style.
    $both
        .mousedown(function () {
            if (!clickerController.enabled)
                return;

            clicker.click();
            player.update();
            scoreController.update();

            var rtop = (Math.random() - 0.5) * 75;
            var rleft = (Math.random() - 0.5) * 75;
            $clickAmount.text("+" + format.number(clicker.bpc()));
            $clickAmount.css({ top: rtop, left: rleft });

            if (!faded) {
                faded = true;
                $clicker.fadeTo(400, 0.5);
                $clickAmount.fadeIn();
            }
        })
        .mouseup(function () {
            if (waitHandle)
                clearTimeout(waitHandle);

            waitHandle = timeout(1, function () {
                $clicker.fadeTo(400, 1);
                $clickAmount.fadeOut();
                faded = false;
                waitHandle = null;
            });
        });

    return clickerController;
});