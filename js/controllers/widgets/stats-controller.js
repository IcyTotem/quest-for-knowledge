﻿define([
    "core/format",
    "models/singleton/player",
    "models/singleton/clicker",
    "models/instances/resources",
    "models/containers/achievements"
], function (format, player, clicker, resources, achievements) {
    var $stats = $("#stats");
    var $btogo = $stats.find(".b-to-go");
    var $league = $stats.find(".league");
    var $playtime = $stats.find(".playtime");
    var $totalData = $stats.find(".total-data");
    var $totalFlops = $stats.find(".total-flops");
    var $totalClicks = $stats.find(".total-clicks");

    var $achList = $stats.find(".achievements");
    var $achModel = $achList.find("li");

    $achModel.hide();

    var statsController = {
        update: function () {
            if (!$stats.is(":visible"))
                return;

            $league.text(player.league.name);
            $btogo.text(format.number(player.bytesToNextLevel));
            $playtime.text(format.time(player.playtime()));
            $totalData.text(resources.bytes.totalAsString(4));
            $totalFlops.text(resources.flops.totalAsString(4));
            $totalClicks.text(clicker.clicks);

            var $achItems = $achList.find("li");

            if ($achItems.size() - 1 < achievements.unlocked.length) {
                for (var i = $achItems.size() - 1; i < achievements.unlocked.length; ++i) {
                    var achievement = achievements.unlocked[i];
                    var $li = $achModel.clone();

                    $li.find(".description").html(achievement.description);
                    $li.find(".datetime").text(format.date(achievement.datetime));
                    $li.find(".reward").text(format.number(achievement.reward, "Flop"));

                    $achList.append($li);
                    $li.fadeIn();
                }
            }
        }
    };

    return statsController;
});