﻿define([
    "core/factory",
    "core/format",
    "models/containers/notifications",
    "models/prototypes/timer"
], function (factory, format, notifications, timer) {
    var $notificationBox = $("#notification-box");
    var $achievementBox = $("#achievement-box");

    var $image = $notificationBox.find(".icon");
    var $message = $notificationBox.find(".message");
    var $achDescription = $achievementBox.find(".description");
    var $achReward = $achievementBox.find(".reward");

    var notificationsController = {
        holdTime: 3,
        updateTime: 1,
        
        bypass: function () {
            while (!notifications.isEmpty())
                notifications.pop();
        },

        update: function () {
            if (notifications.isEmpty())
                return;

            this.timer.stop();

            var that = this;
            var n = notifications.pop();
            var $target = this.populate(n);

            $target.fadeIn();

            timeout(this.holdTime, function () {
                $target.fadeOut(400, function () { that.timer.start(); });
            });            
        },
        
        populate: function (n) {
            var $target;

            if (n.isAchievement) {
                $target = $achievementBox;
                if (n.message) $achDescription.html(n.message);
                if (n.reward) $achReward.text(format.number(n.reward, "Flop"));
            } else {
                $target = $notificationBox;
                if (n.message) $message.html(n.message);
                if (n.image) $image.attr("src", n.image);
            }

            return $target;
        },

        start: function () {
            var that = this;

            this.timer = factory.create(timer, {
                basePeriod: that.updateTime,
                tick: function () { that.update(); }
            });

            this.timer.start();
        }
    };

    return notificationsController;
});