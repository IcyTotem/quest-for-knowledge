﻿define(["core/large-numbers"], function (largeNumbers) {
    var format = {
        defaultUnit: "B",
        totalDigits: 3,

        number: function(number, unit, significantDigits) {
            if (!unit)
                unit = this.defaultUnit;

            if (!significantDigits)
                significantDigits = this.totalDigits;

            var prefix = largeNumbers.prefix(number);

            if (prefix.order == 0)
                return (number.toFixed(0) + " " + unit);

            var significantPart = number / prefix.multiplier;
            var integralDigits = largeNumbers.integralDigits(significantPart);
            var decimalDigits = Math.max(0, significantDigits - integralDigits);

            return (significantPart.toFixed(decimalDigits) + " " + prefix.symbol + unit);
        },

        word: function (number, unit, significantDigits) {
            if (!unit)
                unit = this.defaultUnit;

            if (!significantDigits)
                significantDigits = this.totalDigits;

            var suffix = largeNumbers.suffix(number);

            if (suffix.order == 0)
                return (number.toFixed(0) + " " + unit);

            var significantPart = number / suffix.multiplier;
            var integralDigits = largeNumbers.integralDigits(significantPart);
            var decimalDigits = Math.max(0, significantDigits - integralDigits);

            return (significantPart.toFixed(decimalDigits) + " " + suffix.name.toLowerCase() + " " + unit);

        },

        time: function (seconds) {
            if (seconds < 60)
                return (seconds.toFixed(0) + "s");

            var minutes = Math.floor(seconds / 60);
            var trailingSeconds = Math.floor(seconds % 60);

            if (minutes < 60)
                return (minutes.toFixed(0) + "m " + trailingSeconds.toFixed(0) + "s");

            var hours = Math.floor(minutes / 60);
            var trailingMinutes = Math.floor(minutes % 60);

            return (hours.toFixed(0) + "h " + trailingMinutes.toFixed(0) + "m " + trailingSeconds.toFixed(0) + "s");
        },

        date: function (date) {
            var datePart, timePart;
            
            if (date.isToday()) {
                datePart = "today";
            } else if (date.isYesterday()) {
                datePart = "yesterday";
            } else {
                datePart = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
            }

            timePart = date.getHours() + ":" + date.getMinutes();

            return datePart + ", at " + timePart;
        }
    };

    return format;
});