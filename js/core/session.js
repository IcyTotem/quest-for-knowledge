﻿define(["core/factory"], function (factory) {
    var session = {
        has: function (objectName) {
            return localStorage && !isUndefined(localStorage[objectName]);
        },

        load: function (options) {
            var parsedObject;

            if (isString(options.source) && localStorage)
                parsedObject = JSON.parse(localStorage[options.source]);
            else if (!isUndefined(options.source))
                parsedObject = options.source;
            else
                return null;

            if (options.isSingleObject && options.basePrototype)
                return factory.override(options.basePrototype).members(parsedObject);

            if (options.isCollection && options.basePrototype) {
                var result = options.basePrototype;
                var ignoredKeys = options.ignoredKeys || [];
                var ignoredMembers = options.ignoredMembers || [];

                for (key in parsedObject) { 
                    if (ignoredKeys.indexOf(key) >= 0) continue;

                    var instance = result[key];
                    var savedInstance = parsedObject[key];

                    if (!instance || !savedInstance) continue;

                    for (member in savedInstance) {
                        if (ignoredMembers.indexOf(member) >= 0) continue;
                        instance[member] = savedInstance[member];
                    }
                }

                return result;
            }

            return parsedObject;
        },

        save: function (object, name) {
            var all = object.all;
            delete object.all;
            localStorage[name] = JSON.stringify(object);
            object.all = all;
        },

        clear: function () {
            for (key in localStorage)
                delete localStorage[key];
        }
    };

    return session;
});