﻿define([], function () {
    var allPrefixes = [];
    var allSuffixes = [];

    function Prefix(name, symbol, order) {
        this.name = name;
        this.symbol = symbol;
        this.order = order;
        this.multiplier = Math.pow(10, order);

        allPrefixes.push(this);
    }

    function Suffix(name, order) {
        this.name = name;
        this.order = order;
        this.multiplier = Math.pow(10, order);

        allSuffixes.push(this);
    }

    var largeNumbers = {
        prefixes: {
            zero: new Prefix("Zero", "", 0),
            kilo: new Prefix("Kilo", "k", 3),
            mega: new Prefix("Mega", "M", 6),
            giga: new Prefix("Giga", "G", 9),
            tera: new Prefix("Tera", "T", 12),
            peta: new Prefix("Peta", "P", 15),
            exa: new Prefix("Exa", "E", 18),
            zetta: new Prefix("Zetta", "Z", 21),
            yotta: new Prefix("Yotta", "Y", 24),
            bronto: new Prefix("Bronto", "B", 27),
            geop: new Prefix("Geop", "Ge", 30),
            sagan: new Prefix("Sagan", "S", 33),
            pija: new Prefix("Pija", "Pj", 36),
            alpha: new Prefix("Alpha", "A", 39),
            kryat: new Prefix("Kryat", "Kr", 42),
            amos: new Prefix("Amos", "Am", 45),
            pectrol: new Prefix("Pectrol", "Pc", 48),
            bolger: new Prefix("Bolger", "Bg", 51), 
            sambo: new Prefix("Sambo", "Sb", 54),
            quesa: new Prefix("Quesa", "Q", 57),
            kinsa: new Prefix("Kinsa", "Kn", 60),
            ruther: new Prefix("Ruther", "R", 63),
            dubni: new Prefix("Dubni", "D", 66),
            seaborg: new Prefix("Seaborg", "Sr", 69), 
            bohr: new Prefix("Bohr", "Bh", 72),
            hassiu: new Prefix("Hassiu", "H", 75),
            meitner: new Prefix("Meitner", "Mt", 78), 
            darmsta: new Prefix("Darmsta", "Dm", 81),
            roent: new Prefix("Roent", "Rn", 84),
            coper: new Prefix("Coper", "C", 87),
            all: allPrefixes
        },

        suffixes: {
            none: new Suffix("", 0),
            thousand: new Suffix("Thousand", 3),
            million: new Suffix("Million", 6),
            billion: new Suffix("Billion", 9),
            trillion: new Suffix("Trillion", 12),
            quadrillion: new Suffix("Quadrillion", 15),
            quintillion: new Suffix("Quintillion", 18),
            sextillion: new Suffix("Sextillion", 21),
            septillion: new Suffix("Septillion", 24),
            octillion: new Suffix("Octillion", 27),
            nonillion: new Suffix("Nonillion", 30),
            decillion: new Suffix("Decillion", 33),
            undecillion: new Suffix("Undecillion", 36),
            duodecillion: new Suffix("Duodecillion", 39),
            tredecillion: new Suffix("Tredecillion", 42),
            all: allSuffixes
        },

        orderOfMagnitude: function (number) {
            if (number < 1)
                return 0;
            else
                return Math.floor(Math.log10(Math.abs(number)));
        },

        integralDigits: function (number) {
            if (number < 1)
                return 0;
            else
                return Math.floor(Math.log10(Math.abs(number))) + 1;
        },

        level: function (number) {
            return Math.floor(this.orderOfMagnitude(number) / 3);
        },

        prefix: function (number) {
            var index = this.level(number);

            if (index < this.prefixes.all.length)
                return this.prefixes.all[index];
            else
                return this.prefixes.all[this.prefixes.all.length - 1];
        },

        suffix: function (number) {
            var index = this.level(number);

            if (index < this.suffixes.all.length)
                return this.suffixes.all[index];
            else
                return this.suffixes.all[this.suffixes.all.length - 1];
        }
    };

    return largeNumbers;
});