﻿define([], function () {
    var baseline = {
        bps: function (rank) {
            var h = rank - 16;
            var gamma = Math.pow(42, rank - 16);
            var alpha = 10 + 0.56 * rank;
            var beta = 0.4 * Math.pow(rank, 1.22);

            return Math.floor((gamma + rank - 1) * Math.pow(alpha, beta) + Math.pow(rank, 3));                
        },

        cost: function (rank) {
            return Math.floor(this.bps(rank) * 5 * Math.pow(rank, 0.5) * Math.pow(4, 0.9 + 0.15 * (rank - 1)));
        }
    };

    return baseline;
});