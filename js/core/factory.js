﻿define([], function () {
    var factory = {        
        override: function (obj) {
            function getOverrideTable() {
                if (obj.overrideTable)
                    return obj.overrideTable;
                else
                    return (obj.overrideTable = []);
            }

            function getOverrideStack(name) {
                var table = getOverrideTable(obj);

                if (table[name])
                    return table[name];
                else
                    return (table[name] = []);
            }

            return {
                members: function (container) {
                    for (var name in container) {
                        var oldValue = obj[name];
                        var newValue = container[name];

                        if (oldValue) {
                            var overrideStack = getOverrideStack(name);
                            overrideStack.push(oldValue);
                        }

                        obj[name] = newValue;
                    }

                    return obj;
                }
            };
        },

        unoverride: function (obj) {
            if (!obj.overrideTable)
                return;

            return {
                members: function (names) {
                    names.forEach(function (name) {
                        var overrideStack = obj.overrideTable[name];

                        if (!overrideStack)
                            return;

                        if (overrideStack.length > 0) {
                            var oldValue = overrideStack.splice(0, 1)[0];
                            obj[name] = oldValue;
                        }
                    });

                    return obj;
                }
            }  
        },

        create: function (base, properties) {
            var instance = Object.create(base);
            var result = this.override(instance).members(properties);

            if (result.init) {
                result.init();
            }

            if (result.required) {
                result.required.forEach(function (name) {
                    if (isUndefined(result[name]))
                        console.log("Missing '" + name + "' from " + result);
                });
            }

            return result;
        },

        collection: function (obj) {
            var all = [];

            for (var key in obj) {
                var instance = obj[key];
                if (isObject(instance)) all.push(instance);
            }

            obj.all = all;
            return obj;
        }
    };

    return factory;
})