﻿define(["models/containers/achievements"], function (achievements) {
    var hub = {
        required: ["device"],
        count: 0,
        total: 0,
        stack: 1,
        costMultiplier: 1.1,

        init: function () {
            this.stackSizes = [1, 5, 10, 50, 100, 500, 1000];
            this.achievementSizes = [10, 50, 100, 500, 1000];
        },

        bps: function () {
            return this.device.bps() * this.count;
        },

        tick: function (deltaTime) {
            if (!deltaTime)
                deltaTime = 1;

            var production = this.bps() * deltaTime;
            this.total += production;
            return production;
        },

        cost: function (number) {
            if (number <= 0)
                return 0;

            var r = this.costMultiplier;
            var c = this.count;
            var result = this.device.cost() * (Math.geometricSum(r, c + number) - Math.geometricSum(r, c));

            return Math.floor(result);
        },

        stackCost: function () {
            return this.cost(this.stack);
        },

        add: function (number) {
            var oldCount = this.count;
            this.count += number;
            this.checkAchievements(oldCount);
        },

        addStack: function () {
            return this.add(this.stack);
        },

        buy: function (number) {
            var cost = this.cost(number);

            if (this.device.currency.count < cost)
                return false;

            this.device.currency.consume(cost);
            this.add(number);

            return true;
        },

        buyStack: function () {
            return this.buy(this.stack);
        },

        increaseStack: function () {
            var index = this.stackSizes.indexOf(this.stack);

            if (index < 0)
                this.stack = 1;
            else
                this.stack = this.stackSizes[(index + 1) % this.stackSizes.length];
        },

        checkAchievements: function (oldCount) {
            for (var i = 0; i < this.achievementSizes.length; ++i) {
                var notableSize = this.achievementSizes[i];

                if ((oldCount < notableSize) && (this.count >= notableSize)) {
                    achievements.unlock({
                        reward: notableSize,
                        description: "Collect <strong>" + notableSize + "</strong> " + this.device.name + "s"
                    });
                    this.achievementSizes.splice(i--, 1);
                }
            }
        }
    };

    return hub;
});