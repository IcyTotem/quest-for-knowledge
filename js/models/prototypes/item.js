define([], function () {
    var item = {
        required: ["id", "name", "description", "onUse"],
        purchasable: false,
        baseCost: 1,
        baseCostMultiplier: 1,
        count: 0,
        total: 0,
        stack: 1,

        init: function () {
            this.stackSizes = [1, 5, 10, 50, 100, 500];
        },

        gain: function (x) {
            this.count += x;
            this.total += x;
        },

        increaseStack: function () {
            var index = this.stackSizes.indexOf(this.stack);

            if (index < 0) {
                this.stack = 1;
            } else {
                this.stack = this.stackSizes[(index + 1) % this.stackSizes.length];
            }
        },

        use: function (x) {
            var amount = Math.min(x, this.count);

            if (amount == 0)
                return;

            this.count -= amount;
            this.onUse(amount);
        },

        useStack: function () {
            this.use(this.stack);
        },

        onUse: function (amount) {

        },

        details: function () {
            return this.description;
        },

        cost: function (amount) {
            var r = this.baseCostMultiplier;
            var t = this.total;
            var result = this.baseCost * (Math.geometricSum(r, t + amount) - Math.geometricSum(r, t));

            return Math.floor(result);
        },

        stackCost: function () {
            return this.cost(this.stack);
        },

        buy: function (number) {
            var cost = this.cost(number);

            if (!this.currency)
                return console.log("No currency for " + this.name);

            if (this.currency.count < cost)
                return false;

            this.currency.consume(cost);
            this.gain(number);

            return true;
        },

        buyStack: function () {
            return this.buy(this.stack);
        }
    };

    return item;
});