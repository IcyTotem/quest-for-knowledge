﻿define([], function () {
    var achievement = {
        required: ["description", "reward"]
    };

    return achievement;
})