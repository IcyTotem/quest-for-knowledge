define([], function () {
    var timer = {
        required: ["basePeriod", "tick"],
        speed: 1,

        start: function () {
            var that = this;
            this.handle = interval(this.basePeriod / this.speed, function () {
                that.tick();
            });
        },

        stop: function () {
            clearInterval(this.handle);
        },

        speedUp: function (rate) {
            this.stop();
            this.speed = rate;
            this.start();
        },

        slowDown: function (rate) {
            this.stop();
            this.speed = 1 / rate;
            this.start();
        }
    };

    return timer;
});