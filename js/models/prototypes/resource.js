﻿define(["core/format"], function (format) {
    var resource = {
        required: ["id", "name", "description", "unit"],
        count: 0,
        total: 0,
        cached: 0,
        refundCounter: 0,
        refundThreshold: 600,
        discoveryRange: 8,

        gain: function (x) {
            var oldCount = this.count;
            var newCount = this.count + x;

            this.count += x;
            this.total += x;

            // The following part attempts to correct floating-point approximation
            // for large numbers sum. It stores small increments in a cache and when
            // the cache content is big enough to impact the count, it is emptied

            var actualIncrement = newCount - oldCount;
            var remainder = x - actualIncrement;
            this.cached += remainder;

            if (++this.refundCounter >= this.refundThreshold) {
                this.refundCounter = 0;

                if (this.cached + this.count > this.count) {
                    oldCount = this.count;
                    newCount = this.count + this.cached;
                    actualIncrement = newCount - oldCount;
                    remainder = this.cached - actualIncrement;

                    this.count = newCount;
                    this.cached = remainder;
                }
            }
        },

        gainPercent: function (x) {
            this.gain(this.count * x);
        },

        consume: function (x) {
            var amount = Math.min(x, this.count)
            this.count -= amount;
        },

        details: function () {
            return this.description;
        },

        countAsString: function (significantDigits) {
            return format.number(this.count, this.unit, significantDigits);
        },

        totalAsString: function (significantDigits) {
            return format.number(this.total, this.unit, significantDigits);
        }
    };

    return resource;
});