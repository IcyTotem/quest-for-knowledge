﻿define(["core/format"], function (format) {
    var effect = {
        required: ["name", "begin", "end", "label"],

        init: function () {
            this.enabled = false;
            this.startTime = 0;
            this.duration = 1;
        },

        activate: function (duration, period) {
            var that = this;

            if (duration == 0)
                return;

            if (this.enabled) {
                this.duration += duration;
                clearTimeout(this.thandle);

                this.thandle = timeout(this.startTime + this.duration - now(), function () {
                    that.end()
                    that.enabled = false;
                    clearInterval(that.ihandle);
                });
            } else {
                this.begin();
                this.enabled = true;

                this.period = period;
                this.duration = duration;
                this.startTime = now();

                if (period)
                    this.ihandle = interval(period, function () { that.tick(); });

                this.thandle = timeout(duration, function () {
                    that.end()
                    that.enabled = false;
                    clearInterval(that.ihandle);
                });
            }
        },

        progress: function () {
            return 1 - Math.max(0, Math.min(1, (now() - this.startTime) / this.duration));
        },

        label: function () {
            return format.time(this.duration - (now() - this.startTime));
        },

        tick: function () {

        }
    };

    return effect;
})