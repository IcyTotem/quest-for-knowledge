﻿define(["models/containers/achievements"], function (achievements) {
    var upgrade = {
        required: ["target", "name", "description", "effect"],
        baseCostMultiplier: 4,
        level: 0,
        delta: 1,

        init: function () {
            this.baseCost = this.target.device.baseCost * 42;
            this.achievementLevels = [1, 5, 10, 25, 50, 100, 250, 500, 1000];
        },

        apply: function (number) {
            if (!number) number = 1;

            var oldLevel = this.level;
            this.level += number;
            this.effect();
            this.checkAchievements(oldLevel);
        },

        applyDelta: function () {
            this.apply(this.delta);
        },

        increaseDelta: function () {
            if (++this.delta > 10)
                this.delta = 1;
        },

        cost: function (number) {
            var r = this.baseCostMultiplier;
            var l = this.level;
            var result = this.baseCost * (Math.geometricSum(r, l + number) - Math.geometricSum(r, l));

            return Math.floor(result);
        },

        deltaCost: function () {
            return this.cost(this.delta);
        },

        checkAchievements: function (oldLevel) {
            for (var i = 0; i < this.achievementLevels.length; ++i) {
                var achievementLevel = this.achievementLevels[i];

                if ((oldLevel < achievementLevel) && (this.level >= achievementLevel))
                    achievements.unlock({
                        reward: achievementLevel * this.target.device.rank,
                        description: "Upgrade <strong>" + this.name + "</strong> to level " + achievementLevel
                    });
            }
        },

        buy: function (number) {
            var cost = this.cost(number);

            if (this.target.device.currency.count < cost)
                return false;

            this.target.device.currency.consume(cost);
            this.apply(number);

            return true;
        },

        buyDelta: function () {
            this.buy(this.delta);
        }
    };

    return upgrade;
});