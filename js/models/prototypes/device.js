﻿define(["core/baseline"], function (baseline) {
    var device = {
        required: ["name", "rank", "type", "description", "currency"],

        init: function () {
            if (!this.baseBps)  this.baseBps = baseline.bps(this.rank);
            if (!this.baseCost) this.baseCost = baseline.cost(this.rank);
        },

        bps: function () {
            return this.baseBps;
        },

        cost: function () {
            return this.baseCost;
        }
    };

    return device;
});