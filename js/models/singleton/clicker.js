define([
    "core/session",
    "models/instances/resources",
    "models/containers/achievements"
], function (session, resources, achievements) {
    var clicker = {
        baseBpc: 1,
        clicks: 0,

        bpc: function () {
            return Math.max(this.baseBpc, Math.floor(0.17 * Math.sqrt(resources.bytes.total)));
        },

        click: function () {
            var oldClicks = this.clicks++;
            resources.bytes.gain(this.bpc());
            this.checkAchievements(oldClicks);
        },

        checkAchievements: function (oldClicks) {
            if (oldClicks == 0)
                return;

            var oldLog = Math.floor(Math.log10(oldClicks));
            var newLog = Math.floor(Math.log10(this.clicks));
            var that = this;

            if (newLog < 2)
                return;

            if (newLog > oldLog)
                achievements.unlock({
                    reward: Math.pow(5, oldLog),
                    description: "Click <strong>" + that.clicks + "</strong> times"
                });
        },

        load: function (externalSource) {
            session.load({
                source: externalSource || "clicker",
                isSingleObject: true,
                basePrototype: this
            });
        }
    };

    if (session.has("clicker"))
        clicker.load();

    return clicker;
})