﻿define([
    "core/session",
    "core/large-numbers",
    "models/instances/resources",
    "models/containers/achievements"
], function (session, largeNumbers, resources, achievements) {
    var player = {
        level: 0,
        league: largeNumbers.prefixes.zero,
        bytesToNextLevel: 0,
        startTime: now(),

        update: function () {
            var oldLevel = this.level;

            this.level = largeNumbers.level(resources.bytes.total);
            this.league = largeNumbers.prefix(resources.bytes.total);
            this.bytesToNextLevel = this.league.multiplier * 1000 - resources.bytes.total;

            this.checkAchievements(oldLevel);
        },

        playtime: function () {
            return now() - this.startTime;
        },

        checkAchievements: function (oldLevel) {
            var newLevel = this.level;

            if (oldLevel == newLevel)
                return;

            var value, multiplier;

            switch (newLevel % 3) {
                case 0: value = 1; break;
                case 1: value = 2.5; break;
                case 2: value = 5; break;
            }

            multiplier = Math.pow(10, Math.floor(oldLevel / 3) + 1);

            achievements.unlock({
                reward: value * multiplier,
                description: "Reach the <strong>" + this.league.name + " league</strong>"
            });
        },

        load: function (externalSource) {
            session.load({
                source: externalSource || "player",
                isSingleObject: true,
                basePrototype: this
            });
        }
    };

    if (session.has("player"))
        player.load();

    return player;
});