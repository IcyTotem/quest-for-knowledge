﻿define([
    "models/singleton/clicker",
    "models/singleton/player"
], function (clicker, player) {
    return {
        clicker: clicker,
        player: player
    };
});