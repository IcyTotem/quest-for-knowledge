﻿define([
    "core/factory",
    "core/session",
    "models/prototypes/device",
    "models/instances/resources"
], function (factory, session, device, resources) {
    var devices = factory.collection({
        apprentice: factory.create(device, {
            name: "Apprentice",
            rank: 1,
            type: "Human",
            currency: resources.bytes,
            description: "An young apprentice eager to increase and spread his knowledge."
        }),

        elder: factory.create(device, {
            name: "Elder",
            rank: 2,
            type: "Human",
            currency: resources.bytes,
            description: "A wise elder with many years of experience. He knows a lot about many things, but he is also a bit grumpy."
        }),

        stone: factory.create(device, {
            name: "Stone",
            rank: 3,
            type: "Material",
            currency: resources.bytes,
            description: "Stone tablets are solid and persistent: they will allow you to collect data and maintain it for centuries."
        }),

        book: factory.create(device, {
            name: "Book",
            rank: 4,
            type: "Material",
            currency: resources.bytes,
            description: "Books are the new fronteer of information! Your library won't do without them!"
        }),

        tape: factory.create(device, {
            name: "Tape",
            rank: 5,
            type: "Magnetic",
            currency: resources.bytes,
            description: "A magnetic tape on which to record data. Reliable, flexible and veeeeery LONG!"
        }),

        core: factory.create(device, {
            name: "Core",
            rank: 6,
            type: "Magnetic",
            currency: resources.bytes,
            description: "Another piece of magnetic technology, behold the Core!"
        }),

        disc: factory.create(device, {
            name: "Disc",
            rank: 7,
            type: "Optic",
            currency: resources.bytes,
            description: "It's sleek, fast and may cause harm if thrown. No, it's not a shuriken."
        }),

        cd: factory.create(device, {
            name: "Compact Disc",
            rank: 8,
            type: "Optic",
            currency: resources.bytes,
            description: "It's like a disc, but compact! Better laser technology and denser areas allow for even faster data transmission."
        }),

        hd: factory.create(device, {
            name: "Hard Drive",
            rank: 9,
            type: "Tech",
            currency: resources.bytes,
            description: "A spinning compound of magnetic discs with superior annhilation pow... emh... storage capacity."
        }),

        ssd: factory.create(device, {
            name: "Solid State Drive",
            rank: 10,
            type: "Tech",
            currency: resources.bytes,
            description: "A splendid piece of technology, composed by tiny unmovable gates that allow super dense and fast data transfer!"
        }),

        neural: factory.create(device, {
            name: "Neural System",
            rank: 11,
            type: "Organic",
            currency: resources.bytes,
            description: "The power of human mind with the control and precision of a machine: behold the ultimate organic technology!"
        }),

        dna: factory.create(device, {
            name: "DNA",
            rank: 12,
            type: "Organic",
            currency: resources.bytes,
            description: "The smaller, the better! Tons of information encoded in something so small that you can't even see!"
        }),

        hologram: factory.create(device, {
            name: "Hologram",
            rank: 13,
            type: "Luminal",
            currency: resources.bytes,
            description: "Exploits a full-volume light technology to encode even denser information, because three dimensions is megl che two."
        }),

        prism: factory.create(device, {
            name: "Prism",
            rank: 14,
            type: "Luminal",
            currency: resources.bytes,
            description: "This is a piece of alien technology more efficient than holograms. We still don't know how it works precisely, but it GLOWS in the dark!"
        }),

        catalyst: factory.create(device, {
            name: "Energy Catalyst",
            rank: 15,
            type: "Energetic",
            currency: resources.bytes,
            description: "Directly from the Andromeda Galaxy, the cutting-edge technology for data storage based on pure energy! Don't touch it... it's hot."
        }),

        quantum: factory.create(device, {
            name: "Quantum Core",
            rank: 16,
            type: "Energetic",
            currency: resources.bytes,
            description: "This bad boy can transfer almost a billion billion bytes in a second... as long as you don't look at it."
        }),

        timeWinder: factory.create(device, {
            name: "Time Winder",
            rank: 17,
            type: "Spacetime",
            currency: resources.bytes,
            description: "There is no better place to send excess information than the past! Wait... what's all this data coming from? It wasn't here a second ago."
        }),

        blackHole: factory.create(device, {
            name: "Black Hole",
            rank: 18,
            type: "Spacetime",
            currency: resources.bytes,
            description: "That's right, we use black holes to compress tremendous amount of information in a singularity. The only problem is taking it out. We are working on that."
        }),

        plank: factory.create(device, {
            name: "Plank Engine",
            rank: 19,
            type: "Infinity",
            currency: resources.bytes,
            description: "We are now storing information in the very essence of reality, down to spaces so small that even a quark would need a very powerful microscope to see."
        }),

        fractal: factory.create(device, {
            name: "Fractal",
            rank: 20,
            type: "Infinity",
            currency: resources.bytes,
            description: "Oh, here's the photo of my niece! And inside its first pixel is the history of humanity. And inside its first word lies a treaty on martian cusine. And in its first recipe there's the answer to life, the universe and everything else. And inside that, oh well, you got it."
        }),

        improb: factory.create(device, {
            name: "Infinity Archive",
            rank: 21,
            type: "Improbable",
            currency: resources.flops,
            baseCost: 42000,
            description: "It's highly unlikely that a similar device exists, therefore it's highly likely that the infinite improbability 3D printer will print it."
        }),

        load: function (externalSource) {
            session.load({
                source: externalSource || "devices",
                isCollection: true,
                basePrototype: this,
                ignoredKeys: ["all"],
                ignoredMembers: ["currency"]
            });
        }
    });

    if (session.has("devices"))
        devices.load();

    return devices;
});