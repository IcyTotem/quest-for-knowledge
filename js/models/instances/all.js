﻿define([
    "models/instances/devices",
    "models/instances/effects",
    "models/instances/hubs",
    "models/instances/items",
    "models/instances/resources",
    "models/instances/upgrades"
], function (devices, effects, hubs, items, resources, upgrades) {
    return {
        devices: devices,
        effects: effects,
        hubs: hubs,
        items: items,
        resources: resources,
        upgrades: upgrades
    };
});