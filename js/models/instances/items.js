define([
    "core/factory",
    "core/format",
    "core/session",
    "models/prototypes/item", 
    "models/instances/effects", 
    "models/instances/hubs",
    "models/instances/resources",
    "models/containers/notifications"
], function (factory, format, session, item, effects, hubs, resources, notifications) {
    var items = factory.collection({
        magneticCharges: factory.create(item, {
            id: 1,
            name: "Magnetic Charges",
            description: "Speed up all <em>Magnetic</em> devices for a limited time.",
            effect: effects.magneticCharge,
            details: function () {
                return "Speed up all <em>Magnetic</em> devices by <em>69 times</em> for <strong>" + format.time(10) + "</strong>.<br/>" +
                       "Spending more than 1 Magnetic Charge increases the effect duration.";
            },
            onUse: function (amount) {
                effects.magneticCharge.activate(amount * 10);
            }
        }),

        spins: factory.create(item, {
            id: 2,
            name: "Spins",
            description: "Clicking grants bonus data for a limited time.",
            effect: effects.spin,
            details: function () {
                return "You instantly gain <em>20%</em> of your B/s production per click for <strong>" + format.time(10) + "</strong>.<br/>" +
                       "Spending more than 1 Spin increases the effect duration.";
            },
            onUse: function (amount) {
                effects.spin.activate(amount * 10);
            }
        }),

        prismaticRays: factory.create(item, {
            id: 3,
            name: "Prismatic Rays",
            description: "All <em>Luminal</em> devices cost less for a limited time.",
            effect: effects.prismaticRay,
            details: function () {
                return "All <em>Luminal</em> devices cost <em>90%</em> less for <strong>" + format.time(10) + "</strong>.<br/>" +
                       "Spending more than 1 Prismatic Ray increases the effect duration.";
            },
            onUse: function (amount) {
                effects.prismaticRay.activate(amount * 10);
            }
        }),

        hypercharges: factory.create(item, {
            id: 4,
            name: "Hypercharge",
            description: "Permanently increase your <em>Tech</em> devices performances, but may destroy some units in the process.",
            purchasable: true,
            currency: resources.flops,
            baseCost: 80,
            baseCostMultiplier: 2,
            details: function () {
                return "Permanently increase the performance of all your <em>Tech</em> devices by <strong>5 to 20 times</strong>, but " +
                       "each target device has a 30% to overload and fail.";
            }, 
            onUse: function (amount) {
                var multiplier = 5 + 15 * Math.random();
                var deadHds = 0;
                var deadSsds = 0;

                for (var i = 0; i < hubs.hds.count; ++i)
                    deadHds += (Math.random() < 0.3) ? 1 : 0;
                for (var i = 0; i < hubs.ssds.count; ++i)
                    deadSsds += (Math.random() < 0.3) ? 1 : 0;

                hubs.hds.device.baseBps *= multiplier;
                hubs.ssds.device.baseBps *= multiplier;
                hubs.hds.count -= deadHds;
                hubs.ssds.count -= deadSsds;

                notifications.push({
                    image: "images/items/" + this.id + ".png",
                    message: "<em>Tech</em> performance increased by <strong>" + multiplier.toFixed(2) + "</strong>! " +
                             "You lost <strong>" + deadHds + " " + hubs.hds.device.name + "s</strong> and " +
                             "<strong>" + deadSsds + " " + hubs.ssds.device.name + "s</strong>!"
                });
            }
        }),

        androidizer: factory.create(item, {
            id: 5,
            name: "Androidizer",
            description: "Transform your <em>Apprentices</em> into <em>Androids</em>!",
            purchasable: true,
            currency: resources.flops,
            baseCost: 31,
            baseCostMultiplier: 4,
            details: function () {
                if (hubs.apprentices.device.baseBps < 50)
                    return "Your <em>" + hubs.apprentices.device.name + "s</em> become <em>Tech</em> devices and transfer <strong>50 B/s</strong> as base value instead of 1 B/s.";
                else
                    return "Your <em>" + hubs.apprentices.device.name + "s</em> become <em>Tech</em> devices and transfer <strong>50%</strong> more data as base value.";
            },
            onUse: function (amount) {
                hubs.apprentices.device.name = "Android";
                hubs.apprentices.device.description = "Once a young apprentice eager to learn, now a merciless android that will kill you for a grammar mistake.";
                hubs.apprentices.device.type = "Tech";

                if (hubs.apprentices.device.baseBps < 50)
                    hubs.apprentices.device.baseBps = 50;
                else
                    hubs.apprentices.device.baseBps *= 1.5;
            }
        }),

        arcaicReading: factory.create(item, {
            id: 6,
            name: "Arcaic Reading",
            description: "Destroy one <em>" + hubs.books.device.name + "</em> and redistribute its current transfer rate to all other lower rank devices.",
            purchasable: true,
            currency: resources.flops,
            baseCost: 20,
            baseCostMultiplier: 5,
            details: function () {
                return "Destroy one <em>" + hubs.books.device.name + "</em> and redistribute its current transfer rate (" + 
                        format.number(hubs.books.device.bps()) + ") to all other lower rank devices.";
            },
            onUse: function (amount) {
                var destroyedBooks = Math.min(amount, hubs.books.count);

                if (destroyedBooks < amount)
                    this.count += (amount - destroyedBooks);
                
                if (destroyedBooks == 0)
                    return;

                var totalBps = destroyedBooks * hubs.books.device.bps();
                var lowerRankHubs = hubs.all.filter(function (hub) { return hub.device.rank < hubs.books.device.rank; });
                var totalRankSum = 0;
                var message = "<strong>Arcaic Reading</strong>: ";
                var first = true;

                lowerRankHubs.forEach(function (hub) { totalRankSum += hub.device.rank; });
                lowerRankHubs.forEach(function (hub) {
                    var bonusBps = Math.floor(totalBps * hub.device.rank / totalRankSum);
                    hub.device.baseBps += bonusBps;

                    if (!first)
                        message += ", ";

                    message += "<em>" + hub.device.name + "</em> <strong>+" + format.number(bonusBps) + "</strong>";
                    first = false;
                });

                hubs.books.count -= destroyedBooks;

                notifications.push({
                    image: "images/items/" + this.id + ".png",
                    message: message
                });
            }
        }),

        cogversion: factory.create(item, {
            id: 7,
            name: "Cogversion",
            description: "Consume part of your technology to gain some Flops.",
            purchasable: true,
            currency: resources.bytes,
            baseCost: 1e+12,
            details: function () {
                return "Once used, this item will <strong>completely deplete</strong> your data reservoir and will <strong>permanently destroy</strong> 50% of your devices. " +
                       "In return, you will be awarded <strong>" + this.computeFlopBonus() + " Flops</strong>.";
            },
            computeFlopBonus: function () {
                var total = 0;

                hubs.all.forEach(function (hub) {
                    total += Math.floor(Math.pow(hub.count * hub.device.rank, 0.8));
                });

                return total;
            },
            onUse: function (amount) {
                var flopBonus = this.computeFlopBonus();

                hubs.all.forEach(function (hub) {
                    hub.count = Math.floor(hub.count / 2);
                });
                resources.bytes.consume(resources.bytes.count);
                resources.flops.gain(flopBonus);
            }
        }),

        timeTrap: factory.create(item, {
            id: 8,
            name: "Time Trap",
            description: "Continuously <strong>spawn</strong> <em>" + hubs.timeWinders.device.name + "s</em> and <em>" + hubs.blackHoles.device.name + "s</em> while draining data.",
            purchasable: true,
            currency: resources.flops,
            baseCost: 7500,
            baseCostMultiplier: 1.5,
            effect: effects.timeTrap,
            details: function () {
                return "Initially, the item drains <strong>10 bytes</strong> and spawns <strong>1</strong> unit for each <em>Spacetime</em> device. "
                     + "Then, each second the item drains <strong>10 times</strong> more data and spawns <strong>1 additional</strong> unit.";
            },
            onUse: function (amount) {
                effects.timeTrap.activate(amount);
            }
        }),

        nllaser: factory.create(item, {
            id: 9,
            name: "NL-Laser",
            description: "For a limited time, <em>Optic</em> devices stop, but clicking permanently increase their performances.",
            purchasable: true,
            currency: resources.flops,
            baseCost: 100,
            baseCostMultiplier: 2.5,
            effect: effects.nllaser,
            details: function () {
                return "Your <em>Optic</em> devices cease to function for <em>" + format.time(15) + "</em>. During this time, each click you perform "
                     + "permanently increase the transfer rate of those devices by <strong>5%</strong> (up to a maximum of 999%).";
            },
            onUse: function (amount) {
                effects.nllaser.activate(amount * 15);
            }
        }),

        load: function (externalSource) {
            session.load({
                source: externalSource || "items",
                isCollection: true,
                basePrototype: this,
                ignoredKeys: ["all"],
                ignoredMembers: ["currency", "effect"]
            });
        }
    });

    if (session.has("items"))
        items.load();

    return items;
});