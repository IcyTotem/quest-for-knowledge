﻿define([
    "core/factory",
    "core/format",
    "models/prototypes/effect",
    "models/instances/hubs",
    "models/instances/resources",
    "models/singleton/clicker"
], function (factory, format, effect, hubs, resources, clicker) {
    var effects = factory.collection({
        magneticCharge: factory.create(effect, {
            name: "Magnetic Charge",
            multiplier: 69,
            begin: function () {
                hubs.tapes.device.baseBps *= this.multiplier;
                hubs.cores.device.baseBps *= this.multiplier;
            },
            end: function () {
                hubs.tapes.device.baseBps /= this.multiplier;
                hubs.cores.device.baseBps /= this.multiplier;
            }
        }),

        spin: factory.create(effect, {
            name: "Spin",
            begin: function () {
                var percent = 0.2;
                var that = this;

                this.oldBpc = clicker.bpc;

                clicker.bpc = function () {
                    return that.oldBpc.apply(this) + hubs.all.bps() * percent;
                }
            },
            end: function () {
                clicker.bpc = this.oldBpc;
            }
        }),

        prismaticRay: factory.create(effect, {
            name: "Prismatic Ray",
            multiplier: 0.1,
            begin: function () {
                hubs.holograms.device.baseCost *= this.multiplier;
                hubs.prisms.device.baseCost *= this.multiplier;
            },
            end: function () {
                hubs.holograms.device.baseCost /= this.multiplier;
                hubs.prisms.device.baseCost /= this.multiplier;
            }
        }),

        timeTrap: factory.create(effect, {
            name: "Time Trap",
            activate: function (amount) {
                var that = this;

                this.enabled = true;
                this.drain = 10;
                this.spawn = 1;
                this.timeLabel = "Draining " + format.number(10 * amount) + ", spawning " + amount + " units";

                var handle = interval(1, function () {
                    var actualDrain = that.drain * amount;
                    var actualSpawn = that.spawn * amount;

                    if (resources.bytes.count < actualDrain) {
                        clearTimeout(handle);
                        that.enabled = false;
                        return;
                    }

                    resources.bytes.consume(actualDrain);
                    hubs.timeWinders.add(actualSpawn);
                    hubs.blackHoles.add(actualSpawn);

                    that.drain *= 10;
                    that.spawn += 1;
                    that.timeLabel = "Draining " + format.number(that.drain * amount) + ", spawning " + (that.spawn * amount) + " units";
                });
            },
            progress: function () {
                if (resources.bytes.count <= 1)
                    return 0;
                else
                    return Math.max(0, 1 - Math.log10(this.drain) / Math.log10(resources.bytes.count));
            },
            label: function () {
                return this.timeLabel;
            }
        }),

        nllaser: factory.create(effect, {
            name: "NL-Laser",
            begin: function () {
                this.oldDiscBps = hubs.discs.device.bps;
                this.oldCdBps = hubs.cds.device.bps;
                this.startClicks = clicker.clicks;

                hubs.discs.device.bps = function () { return 0; }
                hubs.cds.device.bps = function () { return 0; }
            },
            label: function () {
                return Math.min((clicker.clicks - this.startClicks) * 5, 999) + "% bonus";
            },
            end: function () {
                var bonus = 0.01 * Math.min((clicker.clicks - this.startClicks) * 5, 999);

                hubs.discs.device.baseBps *= 1 + bonus;
                hubs.cds.device.baseBps *= 1 + bonus;

                hubs.discs.device.bps = this.oldDiscBps;
                hubs.cds.device.bps = this.oldCdBps;
            }
        })
    });

    return effects;
});