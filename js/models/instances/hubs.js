﻿define([
    "core/factory",
    "core/session",
    "models/prototypes/hub",
    "models/instances/devices"
], function (factory, session, hub, devices) {
    var hubs = factory.collection({
        apprentices: factory.create(hub, { device: devices.apprentice }),
        elders: factory.create(hub, { device: devices.elder }),
        stones: factory.create(hub, { device: devices.stone }),
        books: factory.create(hub, { device: devices.book }),
        tapes: factory.create(hub, { device: devices.tape }),
        cores: factory.create(hub, { device: devices.core }),
        discs: factory.create(hub, { device: devices.disc }),
        cds: factory.create(hub, { device: devices.cd }),
        hds: factory.create(hub, { device: devices.hd }),
        ssds: factory.create(hub, { device: devices.ssd }),
        neurals: factory.create(hub, { device: devices.neural }),
        dnas: factory.create(hub, { device: devices.dna }),
        holograms: factory.create(hub, { device: devices.hologram }),
        prisms: factory.create(hub, { device: devices.prism }),
        catalysts: factory.create(hub, { device: devices.catalyst }),
        quanta: factory.create(hub, { device: devices.quantum }),
        timeWinders: factory.create(hub, { device: devices.timeWinder }),
        blackHoles: factory.create(hub, { device: devices.blackHole }),
        planks: factory.create(hub, { device: devices.plank }),
        fractals: factory.create(hub, { device: devices.fractal }),
        improbs: factory.create(hub, { device: devices.improb }),
        load: function (externalSource) {
            session.load({
                source: externalSource || "hubs",
                isCollection: true,
                basePrototype: this,
                ignoredKeys: ["all"],
                ignoredMembers: ["device"]
            });
        }
    });

    if (session.has("hubs"))
        hubs.load();

    hubs.all.tick = function (deltaTime) {
        var result = 0;

        this.forEach(function (hub) {
            result += hub.tick(deltaTime);
        });

        return result;
    };

    hubs.all.bps = function () {
        var result = 0;

        this.forEach(function (hub) {
            result += hub.bps();
        });

        return result;
    };

    return hubs;
});