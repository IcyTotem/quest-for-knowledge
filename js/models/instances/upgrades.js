﻿define([
    "core/factory",
    "core/session",
    "core/format",
    "models/prototypes/upgrade",
    "models/instances/hubs",
    "models/instances/resources",
    "models/instances/items",
    "models/singleton/player"
], function (factory, session, format, upgrade, hubs, resources, items, player) {
    var upgrades = factory.collection({
        scolding: factory.create(upgrade, {
            name: "Scolding",
            target: hubs.apprentices,
            description: function (currentLevel) {
                if (this.target.device.baseBps * 0.02 <= 1)
                    return hubs.apprentices.device.name + "s transfer <strong>" + currentLevel + " more B/s</strong> per " + hubs.elders.device.name + ".";
                else
                    return hubs.apprentices.device.name + "s transfer <strong>" + (2 * currentLevel) + "% more B/s</strong> per " + hubs.elders.device.name + ".";
            },
            effect: function () {
                var that = this;
                this.target.device.bps = function () {
                    return this.baseBps + that.level * hubs.elders.count * Math.max(1, this.baseBps * 0.02);
                };
            }
        }),

        council: factory.create(upgrade, {
            name: "Council",
            target: hubs.elders,
            description: function (currentLevel) {
                return hubs.elders.device.name + "s perform <strong>" + (currentLevel * 300) + "%</strong> better.";
            },
            effect: function () {
                var that = this;
                this.target.device.bps = function () {
                    return this.baseBps * (1 + 3 * that.level);
                };
            }
        }),

        solidity: factory.create(upgrade, {
            name: "Solidity",
            target: hubs.stones,
            description: function (currentLevel) {
                return "You only pay <strong>" + (Math.pow(0.5, currentLevel) * 100).toFixed(1) + "%</strong> of normal prices when buying " + hubs.stones.device.name + "s.";
            },
            effect: function () {
                var that = this;
                this.target.device.cost = function () {
                    return this.baseCost * Math.pow(0.5, that.level);
                };
            }
        }),

        book: factory.create(upgrade, {
            name: "Thin paper",
            target: hubs.books,
            description: function (currentLevel) {
                return hubs.books.device.name + "s perform <strong>" + (currentLevel * 200) + "%</strong> better.";
            },
            effect: function () {
                var that = this;
                this.target.device.bps = function () {
                    return this.baseBps * (1 + 2 * that.level);
                };
            }
        }),

        unrolling: factory.create(upgrade, {
            name: "Unrolling",
            target: hubs.tapes,
            description: function (currentLevel) {
                return "You gain <strong>" + currentLevel + " " + items.magneticCharges.name + "</strong> every 3.5 minutes.";
            },
            effect: function () {
                var that = this;

                if (this.handle)
                    clearInterval(this.handle);

                this.handle = interval(210, function () {
                    items.magneticCharges.gain(that.level);
                });
            }
        }),

        powerCore: factory.create(upgrade, {
            name: "Power Core",
            target: hubs.cores,
            description: function (currentLevel) {
                return hubs.cores.device.name + "s perform <strong>" + (25 * currentLevel) + "%</strong> better for each <em>prestige league</em> you advanced.";
            },
            effect: function () {
                var that = this;
                this.target.device.bps = function () {
                    return this.baseBps * (1 + 0.25 * that.level * player.level);
                };
            }
        }),

        doubleFaced: factory.create(upgrade, {
            name: "Double-faced",
            target: hubs.discs,
            description: function (currentLevel) {
                return hubs.discs.device.name + "s perform <strong>" + (100 * currentLevel) + "%</strong> better.";
            },
            effect: function () {
                var that = this;
                this.target.device.bps = function () {
                    return this.baseBps * (1 + that.level);
                };
            }
        }),

        beCompact: factory.create(upgrade, {
            name: "Be compact!",
            target: hubs.cds,
            description: function (currentLevel) {
                return "You have a <strong>" + (2 * currentLevel) + "% chance per minute</strong> to get a new " + hubs.cds.device.name + " for free.";
            },
            effect: function () {
                var that = this;

                if (this.handle)
                    clearInterval(this.handle);

                this.handle = interval(60, function () {
                    if (Math.random() < 0.02 * that.level)
                        hubs.cds.add(1);
                });
            }
        }),

        hardTimes: factory.create(upgrade, {
            name: "Hard Times",
            target: hubs.hds,
            description: function (currentLevel) {
                return "You gain <strong>" + (2 * currentLevel) + " " + items.spins.name + "</strong> per <em>prestige league</em> every 5 minutes."
            },
            effect: function () {
                var that = this;
                var counter = 0;

                if (this.handle)
                    clearInterval(this.handle);

                this.handle = interval(300, function () {
                    items.spins.gain(2 * that.level * player.level);
                });
            }
        }),

        solidConviction: factory.create(upgrade, {
            name: "Solid Conviction",
            target: hubs.ssds,
            description: function (currentLevel) {
                return hubs.ssds.device.name + "s perform <strong>" + (5 * currentLevel) + "%</strong> better for each " + items.spins.name + " you have.";
            },
            effect: function () {
                var that = this;
                this.target.device.bps = function () {
                    return this.baseBps * (1 + 0.05 * that.level * items.spins.count);
                };
            }
        }),

        brainSynergy: factory.create(upgrade, {
            name: "Brain Synergy",
            target: hubs.neurals,
            description: function (currentLevel) {
                return hubs.neurals.device.name + "s perform <strong>" + (Math.pow(1.5, currentLevel) - 1).toFixed(2) + "%</strong> better for each Human unit you have.";
            },
            effect: function () {
                var that = this;
                this.target.device.bps = function () {
                    return this.baseBps * (1 + 0.01 * (Math.pow(1.5, that.level) - 1) * (hubs.apprentices.count + hubs.elders.count));
                };
            }
        }),

        mutation: factory.create(upgrade, {
            name: "Mutation",
            target: hubs.dnas,
            bonus: 0,
            description: function (currentLevel) {
                return "You have a <strong>" + (currentLevel * 5) + "% chance per 5 minutes</strong> to permanently increase " + hubs.dnas.device.name + " performance by 25%";
            },
            effect: function () {
                var that = this;

                if (this.handle)
                    clearInterval(this.handle);

                this.handle = interval(300, function () {
                    if (Math.random() < 0.05 * that.level)
                        that.bonus += 0.25;
                });

                this.target.device.bps = function () {
                    return this.baseBps * (1 + that.bonus);
                };
            }
        }),

        hologram: factory.create(upgrade, {
            name: "Multiplicity",
            target: hubs.holograms,
            description: function (currentLevel) {
                return hubs.holograms.device.name + "s perform <strong>" + (currentLevel * 10) + "%</strong> better for each Hologram you have.";
            },
            effect: function () {
                var that = this;
                this.target.device.bps = function () {
                    return this.baseBps * (1 + 0.1 * that.level * hubs.holograms.count);
                };
            }
        }),

        reflection: factory.create(upgrade, {
            name: "Reflection",
            target: hubs.prisms,
            description: function (currentLevel) {
                return "You gain <strong>" + (3 * currentLevel) + " " + items.prismaticRays.name + "</strong> every 3 minutes.";
            },
            effect: function () {
                var that = this;

                if (this.handle)
                    clearInterval(this.handle);

                this.handle = interval(180, function () {
                    items.prismaticRays.gain(3 * that.level);
                });
            }
        }),

        universalCatalyst: factory.create(upgrade, {
            name: "Universal Catalyst",
            target: hubs.catalysts,
            description: function (currentLevel) {
                return "Gain <strong>" + (5 * currentLevel) + " Flops</strong> every 3 minutes."
            },
            effect: function () {
                var that = this;

                if (this.handle)
                    clearInterval(this.handle);

                this.handle = interval(180, function () {
                    resources.flops.gain(5 * that.level);
                });
            }
        }),

        everythingMatters: factory.create(upgrade, {
            name: "Everything Matters",
            target: hubs.quanta,
            description: function (currentLevel) {
                return hubs.quanta.device.name + "s perform <strong>" + currentLevel + "%</strong> better for every device unit you have."
            },
            effect: function () {
                var that = this;
                this.target.device.bps = function () {
                    var totalCount = 0;
                    hubs.all.forEach(function (hub) { totalCount += hub.count; });
                    return this.baseBps * (1 + 0.01 * that.level * totalCount);
                };
            }
        }),

        warp: factory.create(upgrade, {
            name: "Warp",
            target: hubs.timeWinders,
            description: function (currentLevel) {
                return hubs.timeWinders.device.name + "s' performances increase by <strong>" + (5 * Math.geometricSum(0.95, currentLevel)).toFixed(2)
                     + "%</strong> every 10 minutes. This effect is retroactive."
            },
            effect: function () {
                var that = this;
                var coef = 0.05 * Math.geometricSum(0.95, this.level);

                this.target.device.bps = function () {
                    return this.baseBps * (1 + coef * that.times);
                };

                if (this.handle)
                    clearInterval(this.handle);

                if (!this.times)
                    this.times = 1;

                this.handle = interval(600, function () { that.times++; });
            }
        }),

        radiation: factory.create(upgrade, {
            name: "Radiation",
            target: hubs.blackHoles,
            description: function (currentLevel) {
                return "You have a 20% chance per minute to gain <strong>" + currentLevel + "</strong> random devices for free.";
            },
            effect: function () {
                var that = this;
                
                if (this.handle)
                    clearInterval(this.handle);

                this.handle = interval(60, function () {
                    if (Math.random() >= 0.2)
                        return;

                    for (var i = 0; i < that.level; ++i) {
                        var randomIndex, randomHub;

                        // Only spawn lower rank devices
                        do {
                            randomIndex = Math.randomInt(0, hubs.all.length);
                            randomHub = hubs.all[randomIndex];
                        } while (randomHub.device.rank >= that.target.device.rank);

                        randomHub.add(1);
                    }
                });
            }
        }),

        plankCompression: factory.create(upgrade, {
            name: "Plank Compression",
            target: hubs.planks,
            description: function (currentLevel) {
                return "Reduces the cost of all devices by <strong>" + (100 * (1 - Math.pow(1.7, -currentLevel))).toFixed(4) + "%</strong>.";
            },
            effect: function () {
                var that = this;

                hubs.all.forEach(function (hub) {
                    // Does not apply to Infinity Archive
                    if (hub.device.rank == 21)
                        return;

                    hub.device.cost = function () {
                        return Math.max(1, this.baseCost * Math.pow(1.7, -that.level));
                    };
                });
            }
        }),

        infinityGrasp: factory.create(upgrade, {
            name: "Infinity Grasp",
            target: hubs.fractals,
            description: function (currentLevel) {
                return "All your devices perform <strong>" + Math.pow(5, Math.pow(currentLevel, 0.68)).toFixed(0) + " times</strong> better."
            },
            effect: function () {
                var that = this;

                hubs.all.forEach(function (hub) {
                    if (that.level == 1)
                        hub.device.oldBaseBps = hub.device.baseBps;

                    hub.device.baseBps = hub.device.oldBaseBps * Math.pow(5, Math.pow(that.level, 0.68));
                });
            }
        }),

        load: function (externalSource) {
            session.load({
                source: externalSource || "upgrades",
                isCollection: true,
                basePrototype: this,
                ignoredKeys: ["all"],
                ignoredMembers: ["target"]
            });

            // Then call the effect associated to each leveled upgrades to restore it
            for (key in this) {
                var instance = this[key];
                if (instance.level > 0) instance.effect();
            }
        }
    });

    if (session.has("upgrades"))
        upgrades.load();

    return upgrades;
});