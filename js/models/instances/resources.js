﻿define([
    "core/factory",
    "core/session",
    "models/prototypes/resource"
], function (factory, session, resource) {
    var resources = factory.collection({
        bytes: factory.create(resource, {
            id: 0,
            name: "Bytes",
            description: "Used to pay for everything. This is your primary resource.",
            unit: "B",
            discoveryRange: 8,
        }),

        flops: factory.create(resource, {
            id: 1,
            name: "Flops",
            description: "Special currency awarded for completing tasks and unlocking achievements.",
            unit: "Flop",
            discoveryRange: 1.5
        }),

        load: function (externalSource) {
            session.load({
                source: externalSource || "resources",
                isCollection: true,
                basePrototype: this,
                ignoredKeys: ["all"]
            });
        }
    });

    if (session.has("resources"))
        resources.load();

    return resources;
});