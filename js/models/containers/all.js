﻿define([
    "models/containers/achievements",
    "models/containers/notifications"
], function (achievements, notifications) {
    return {
        achievements: achievements,
        notifications: notifications
    };
});