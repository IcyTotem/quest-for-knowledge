﻿define([
    "core/factory",
    "core/session",
    "models/prototypes/achievement",
    "models/instances/resources",
    "models/containers/notifications"
], function (factory, session, achievement, resources, notifications) {
    var achievements = {
        unlocked: [],

        unlock: function (properties) {
            var instance = factory.create(achievement, properties);
            instance.datetime = new Date();

            resources.flops.gain(instance.reward);

            notifications.push({
                isAchievement: true,
                reward: instance.reward,
                message: instance.description
            });

            this.unlocked.push(instance);
        },

        load: function (externalSource) {
            session.load({
                source: externalSource || "achievements",
                isSingleObject: true,
                basePrototype: this
            });

            for (var i = 0; i < this.unlocked.length; ++i)
                this.unlocked[i].datetime = new Date(this.unlocked[i].datetime);
        }
    };

    if (session.has("achievements"))
        achievements.load();

    return achievements;
});