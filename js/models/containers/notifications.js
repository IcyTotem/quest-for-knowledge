﻿define(["core/session"], function (session) {
    var notifications = {
        list: [],

        push: function (n) {
            this.list.push(n);
        },

        pop: function () {
            if (this.list.length == 0)
                return null;

            var spliced = this.list.splice(0, 1);
            return spliced[0];
        },

        size: function () {
            return this.list.length;
        },

        isEmpty: function () {
            return (this.list.length == 0);
        },

        load: function (externalSource) {
            session.load({
                source: externalSource || "notifications",
                isSingleObject: true,
                basePrototype: this
            });
        }
    };

    if (session.has("notifications"))
        notifications.load();

    return notifications;
});