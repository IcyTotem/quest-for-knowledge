﻿require([
    "core/factory",
    "core/session",
    "models/instances/all",
    "models/containers/all",
    "models/singleton/all",
    "models/prototypes/timer",
    "controllers/lists/all",
    "controllers/widgets/all"
], function (factory, session, instances, containers, singleton, timer, lists, widgets) {
    var gameTimer = factory.create(timer, {
        basePeriod: 1,
        tick: function () {
            instances.resources.bytes.gain(instances.hubs.all.tick(this.basePeriod));
        }
    });

    var uiTimer = factory.create(timer, {
        basePeriod: 1,
        tick: function () {
            singleton.player.update();
            widgets.score.update();
            widgets.stats.update();
            lists.hubs.invalidate();
            lists.upgrades.invalidate();
            lists.resources.invalidate();
            lists.inventory.invalidate();
            lists.shop.invalidate();
        }
    });

    gameTimer.start();
    uiTimer.start();

    $(function () {
        widgets.tooltip.attach();
        widgets.tab.attach();
        widgets.notifications.start();

        lists.hubs.invalidate();

        $(window).unload(function () {
            if (singleton.player.reset)
                return;

            session.save(instances.devices, "devices");
            session.save(instances.hubs, "hubs");
            session.save(instances.items, "items");
            session.save(instances.resources, "resources");
            session.save(instances.upgrades, "upgrades");
            session.save(containers.achievements, "achievements");
            session.save(containers.notifications, "notifications");
            session.save(singleton.clicker, "clicker");
            session.save(singleton.player, "player");
        });
    });
});